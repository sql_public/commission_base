BEGIN
  EXECUTE IMMEDIATE 'DROP VIEW ' || 'vw_commission_base'
    || ' CASCADE CONSTRAINTS';
EXCEPTION
  WHEN OTHERS THEN
    IF SQLCODE != -942 THEN
      RAISE;
    END IF;
END;

CREATE OR REPLACE VIEW vw_commission_base (
spn_dtl_spn_code,
spn_code,
spn_cmp,
spn_cmc,
spn_enm,
spn_ovr_spn_code,
spn_ovr_commission_code,
spn_eng_commission_code,
spn_ovr_cmc,
spn_commission_code,
spn_det_commission_code,
agent_code_cmp,
agent_name,
agent_code,
agent_commission_code,
year,
quarter,
invc_number,
inx_auto_key,
ind_original,
route_desc,
inz_auto_key,
invc_date_as_date,
invc_date,
sales_category,
categories,
kyc_today,
last_kyc,
valid_till,
kyc_q, 
spn_no_of_transactions,
enm_no_of_transactions,
sale_type,
indx_agn,
indx_spn,
route_code,
so_employee,
invoice_route_code,
post_desc,
cmz_auto_key,
company_name,
cmp_parent,
company_code,
pn,
description,
item_number,
qty_ship,
unit_price,
total_invoice,
not_paid_revenue,
unit_cost,
gp,
total_cost, 
total_gp,
sales_person_code,
sales_person_name,
spn_hdr_commission_code,
  CONSTRAINT vw_commission_base_pkey PRIMARY KEY (inx_auto_key) RELY DISABLE NOVALIDATE
) AS
WITH base AS (
SELECT
    cmp.cmz_auto_key AS cmz_auto_key,
    cmp.company_name AS company_name,                                          
    cmp.cmp_parent AS cmp_parent,  
    cmp.company_code AS company_code,
    ind.inx_auto_key AS inx_auto_key,                                          
    ind.ind_original AS ind_original,
    ind.sox_auto_key AS sox_auto_key,
    ind.item_number AS item_number,                                             
    ind.qty_ship AS qty_ship,                                                   
    ind.unit_cost AS unit_cost,                                                                     
    ind.unit_price AS unit_price,                                                
    ind.scz_auto_key AS sales_category,                                             
    ind.route_code AS invoice_route_code,
    ind.route_desc AS route_desc,
    inh.inz_auto_key AS inz_auto_key,
    inh.invc_number AS invc_number,                                            
    inh.invc_date AS invc_date_as_date,
    inh.foreign_amount AS foreign_amount,
    inh.foreign_pmt_amt AS foreign_pmt_amt,
    inh.post_desc AS post_desc,
    to_char(inh.invc_date, 'YYYY.MM.DD') AS invc_date,
    to_char(inh.invc_date, 'YYYY') AS year,
    to_char(inh.invc_date, 'Q') AS quarter,
    spn_dtl.sales_person_code AS spn_dtl_spn_code,    
    spn_dtl.cmc_auto_key AS spn_commission_code,                              
    spn_dtl.sales_person_name AS ind_sales_person_name,
    spn.sales_person_name AS inh_sales_person_name,
    CASE 
        WHEN ind.cmc_auto_key IN (1, 2) THEN sod_spn.sales_person_code 
        WHEN ind.cmc_override IN (1, 2) THEN sod_override.sales_person_code
    END AS spn_code,                   
    spn_cmp.sales_person_code AS spn_cmp,                                       
    cmc_det.cmc_auto_key AS spn_cmc,                                            
    cmc_det_ovr.cmc_auto_key AS spn_ovr_cmc,                                       
    spn_enm.sales_person_code AS spn_enm,                                        
    agn.agent_name AS agent_name,                                               
    agn.agent_code AS agent_code,                                               
    agn.agent_comm AS agent_commission_code,                                               
    agn_cmp.agent_code AS agent_code_cmp,                                       
    spn_ovr.sales_person_code AS spn_ovr_spn_code,   
    cmc_det_ovr.comm_rate AS spn_ovr_commission_code,                
    cmc_det.comm_rate AS spn_det_commission_code,                                      
    cmc_eng.comm_rate AS spn_eng_commission_code, 
    cmc.comm_rate AS spn_hdr_commission_code,
    sod.route_code AS route_code,   
    so_employee.employee_code AS so_employee,
    warehouse_reservations.stm_auto_key AS stm_auto_key,
    pnm.pn AS pn,
    pnm.charge AS charge,
    pnm.description AS description,
    sale_category_codes.so_category_code AS categories
FROM 
    invc_head inh
JOIN invc_detail ind ON inh.inz_auto_key = ind.inz_auto_key
JOIN company cmp ON inh.cmz_auto_key = cmp.cmz_auto_key
JOIN parts pnm ON ind.pnz_auto_key = pnm.pnz_auto_key
JOIN sale_category_codes ON ind.scz_auto_key = sale_category_codes.scz_auto_key
LEFT JOIN (
    SELECT 
        inx_auto_key,
        MAX(stm_auto_key) AS stm_auto_key
    FROM 
        warehouse_reservations
    WHERE warehouse_reservations.entry_date > add_months(sysdate, - 9)
    AND inx_auto_key IS NOT NULL
    GROUP BY inx_auto_key
    ) warehouse_reservations ON ind.inx_auto_key = warehouse_reservations.inx_auto_key
LEFT JOIN so_detail sod ON ind.sox_auto_key = sod.sox_auto_key
LEFT JOIN so_head ON sod.soz_auto_key = so_head.soz_auto_key
LEFT JOIN sys_user so_employee ON so_head.sys_auto_key = so_employee.sys_auto_key
LEFT JOIN sales_person spn ON inh.spn_auto_key = spn.spn_auto_key
LEFT JOIN sales_person spn_dtl ON ind.spn_auto_key = spn_dtl.spn_auto_key
LEFT JOIN sales_person spn_ovr ON ind.spn_override = spn_ovr.spn_auto_key
LEFT JOIN sales_person spn_cmp ON spn_cmp.spn_auto_key = cmp.spn_auto_key
LEFT JOIN sales_person spn_enm ON ind.spn_auto_key2 = spn_enm.spn_auto_key
LEFT JOIN sales_person sod_spn ON sod.spn_auto_key = sod_spn.spn_auto_key
LEFT JOIN sales_person sod_override ON sod.spn_override = sod_override.spn_auto_key
LEFT JOIN comm_codes cmc_det ON ind.cmc_auto_key = cmc_det.cmc_auto_key                    
LEFT JOIN comm_codes cmc_det_ovr ON ind.cmc_override = cmc_det_ovr.cmc_auto_key   
LEFT JOIN comm_codes cmc_eng ON ind.cmc_auto_key2 = cmc_eng.cmc_auto_key 
LEFT JOIN comm_codes cmc ON spn.cmc_auto_key = cmc.cmc_auto_key
LEFT JOIN agent agn_cmp ON cmp.agn_auto_key = agn_cmp.agn_auto_key
LEFT JOIN agent agn ON ind.agn_auto_key = agn.agn_auto_key
WHERE inh.invc_date > add_months(sysdate, -9)
AND ind.scz_auto_key IN (1, 2, 3, 4)
AND UPPER(ind.route_desc) NOT LIKE ('CORE CREDIT')
),

credit_memo_exc AS (
            SELECT 
                credit_memo_info.inx_auto_key, 
                credit_memo_info.so_number, 
                credit_memo_info.soz_auto_key sohs, 
                sodd.soz_auto_key, 
                sodd.sox_auto_key AS exchange_sod, 
                podas.route_code, 
                podas.core_returned, 
                EXCHANGE.po_cost_method AS po_cost
            FROM 
              ( 
                SELECT 
                    invc_detail.inx_auto_key, 
                    ind_orig.sox_auto_key AS sod_orig, 
                    so_head.so_number, 
                    so_detail.soz_auto_key
                 FROM 
                    rma_log
                JOIN invc_head ON rma_log.cminz_auto_key = invc_head.inz_auto_key
                LEFT JOIN invc_detail ON invc_head.inz_auto_key = invc_detail.inz_auto_key
                LEFT JOIN so_detail ON invc_detail.sox_auto_key = so_detail.sox_auto_key
                LEFT JOIN so_head ON so_detail.soz_auto_key = so_head.soz_auto_key
                LEFT JOIN warehouse ON invc_detail.inx_auto_key = warehouse.inx_auto_key
                LEFT JOIN invc_head invc_orig ON rma_log.inz_auto_key = invc_orig.inz_auto_key
                LEFT JOIN invc_detail ind_orig ON invc_orig.inz_auto_key = ind_orig.inz_auto_key
                LEFT JOIN so_detail so_orig ON ind_orig.sox_auto_key = so_orig.sox_auto_key
                WHERE invc_head.invc_date > add_months(sysdate, -9)
                AND invc_detail.route_code = 'M'
                AND invc_detail.scz_auto_key IN (1, 2, 3, 4)
                AND ind_orig.pnz_auto_key = invc_detail.pnz_auto_key
                AND so_orig.item_number = so_detail.item_number
                AND cminz_auto_key IS NOT NULL
                AND stm_auto_key IS NOT NULL       
             ) credit_memo_info 
                 LEFT JOIN (SELECT 
                                sod_sub.soz_auto_key,  
                                sod_sub.sox_auto_key
                            FROM 
                                so_detail sod_sub
                            WHERE sod_sub.entry_date > add_months(sysdate, -9)
                            AND sod_sub.route_code = 'E'                                
                 ) sodd ON credit_memo_info.sod_orig = sodd.sox_auto_key
                 LEFT JOIN (SELECT 
                                MAX(exc_auto_key) max_exc, 
                                sox_auto_key 
                            FROM 
                                EXCHANGE 
                            WHERE EXCHANGE.core_due_date > add_months(sysdate, -9)
                            AND sox_auto_key IS NOT NULL   
                            GROUP BY sox_auto_key
                ) exx ON sodd.sox_auto_key = exx.sox_auto_key 
                LEFT JOIN EXCHANGE ON exx.max_exc = EXCHANGE.exc_auto_key
                LEFT JOIN ( SELECT 
                                core_received.sox_auto_key, 
                                warehouse.pod_auto_key, 
                                podi.route_code, 
                                podi.unit_cost, 
                                CASE WHEN COALESCE(core_po.route_code,'E') = 'P' THEN 1 ELSE 0 END AS core_returned, 
                                warehouse.stm_auto_key
                             FROM
                                (SELECT DISTINCT 
                                        str1.sox_auto_key, 
                                        MAX(str1.stm_auto_key) max_stm
                                FROM 
                                    warehouse_reservations str1
                                WHERE entry_date > add_months(sysdate, -9)
                                GROUP BY str1.sox_auto_key) core_received
                LEFT JOIN warehouse ON core_received.max_stm = warehouse.stm_auto_key
                LEFT JOIN po_detail podi ON warehouse.pod_auto_key = podi.pod_auto_key
                LEFT JOIN (SELECT MAX(exc_auto_key) maxas_e , pod_auto_key FROM EXCHANGE WHERE pod_auto_key IS NOT NULL AND stm_auto_key IS NOT NULL GROUP BY pod_auto_key ) exc_max ON podi.pod_auto_key = exc_max.pod_auto_key
                LEFT JOIN EXCHANGE ON exc_max.maxas_e = EXCHANGE.exc_auto_key
                LEFT JOIN warehouse core_warehouse ON EXCHANGE.stm_auto_key = core_warehouse.stm_auto_key
                LEFT JOIN po_detail core_po ON core_warehouse.pod_auto_key = core_po.pod_auto_key
                ) podas ON sodd.sox_auto_key=podas.sox_auto_key
                WHERE
                sodd.soz_auto_key IS NOT NULL),
                
exc_type AS (SELECT 
                so_detail.sox_auto_key, 
                exc.sox_auto_key AS ex_sod
              FROM
                so_detail 
              JOIN so_head ON so_head.soz_auto_key = so_detail.soz_auto_key
              LEFT JOIN 
                (SELECT DISTINCT sox_auto_key
                    FROM 
                        EXCHANGE
                    WHERE (1=1)
                     AND udc_exc_001 = 'T'
                ) exc ON so_detail.sox_auto_key = exc.sox_auto_key),
                
pod_code AS (
SELECT 
    core_reservation.sox_auto_key AS sox_auto_key, 
    warehouse.pod_auto_key, 
    podi.route_code, 
    warehouse.stm_auto_key, 
    podi.unit_cost, 
    CASE 
        WHEN COALESCE(core_po.route_code,'E') = 'P' THEN 1 ELSE 0 
    END AS core_returned
    FROM
    (SELECT
        str1.sox_auto_key, 
        MAX(str1.stm_auto_key) max_stm
FROM 
    warehouse_reservations str1
WHERE str1.entry_date > add_months(sysdate, -9)
GROUP BY str1.sox_auto_key) core_reservation
LEFT JOIN warehouse warehouse ON core_reservation.max_stm = warehouse.stm_auto_key
LEFT JOIN po_detail podi ON warehouse.pod_auto_key = podi.pod_auto_key
LEFT JOIN (SELECT MAX(EXCHANGE.exc_auto_key) maxas_e, EXCHANGE.pod_auto_key 
            FROM EXCHANGE 
            WHERE EXCHANGE.core_due_date > add_months(sysdate, -9)
            AND EXCHANGE.pod_auto_key IS NOT NULL 
            AND EXCHANGE.stm_auto_key IS NOT NULL 
            GROUP BY EXCHANGE.pod_auto_key ) exc_max ON podi.pod_auto_key = exc_max.pod_auto_key
LEFT JOIN EXCHANGE ON exc_max.maxas_e = EXCHANGE.exc_auto_key
LEFT JOIN warehouse core_warehouse ON EXCHANGE.stm_auto_key = core_warehouse.stm_auto_key
LEFT JOIN po_detail core_po ON core_warehouse.pod_auto_key = core_po.pod_auto_key
),

adj AS (SELECT 
    adjd.inx_auto_key,
    'T' AS j
FROM 
    invc_detail adjd
WHERE adjd.invc_date > add_months(sysdate, - 9)
AND adjd.route_code <> 'J' 
AND EXISTS
(SELECT 
    indj.inz_auto_key,
    indj.item_number
FROM 
    invc_detail indj
WHERE indj.invc_date > add_months(sysdate, - 9)
AND indj.route_code = 'J'
AND indj.unit_cost <> 0 
AND adjd.inz_auto_key = indj.inz_auto_key
AND adjd.item_number = indj.item_number
)
UNION 
SELECT 
    adjd.inx_auto_key,
    'T' AS j
FROM 
    invc_detail adjd
WHERE adjd.invc_date > add_months(sysdate, - 9)
AND adjd.route_code = 'J' 
AND adjd.unit_price <> 0
AND EXISTS
(SELECT 
    indj.inz_auto_key,
    indj.item_number
FROM 
    invc_detail indj
WHERE indj.invc_date > add_months(sysdate, - 9)
AND indj.route_code = 'J'
AND indj.unit_price <> 0 
AND indj.route_code = 'J'
AND adjd.inz_auto_key = indj.inz_auto_key
AND adjd.item_number = indj.item_number
)),

credit_memo_ind AS (SELECT 
    invc_head.invc_number, 
    invc_detail.inx_auto_key, 
    invc_orig.invc_number invc_orig,
    ind_orig.sox_auto_key AS sod_orig, 
    so_head.so_number, 
    so_detail.soz_auto_key,
    ind_orig.unit_cost unit_cost_orig
FROM 
    rma_log
JOIN invc_head ON rma_log.cminz_auto_key = invc_head.inz_auto_key
JOIN invc_detail ON invc_head.inz_auto_key = invc_detail.inz_auto_key
LEFT JOIN so_detail ON invc_detail.sox_auto_key = so_detail.sox_auto_key
LEFT JOIN so_head ON so_detail.soz_auto_key = so_head.soz_auto_key
LEFT JOIN invc_head invc_orig ON rma_log.inz_auto_key = invc_orig.inz_auto_key
LEFT JOIN invc_detail ind_orig ON invc_orig.inz_auto_key = ind_orig.inz_auto_key
LEFT JOIN so_detail so_orig ON ind_orig.sox_auto_key = so_orig.sox_auto_key
WHERE invc_head.invc_date > add_months(sysdate, -12)
AND invc_detail.route_code = 'M'
AND ind_orig.pnz_auto_key = invc_detail.pnz_auto_key
AND so_orig.item_number = so_detail.item_number
AND cminz_auto_key IS NOT NULL)

SELECT 
    base.spn_dtl_spn_code,
    base.spn_code,
    base.spn_cmp,
    base.spn_cmc,
    base.spn_enm,
    base.spn_ovr_spn_code,
    base.spn_ovr_commission_code,
    base.spn_eng_commission_code,
    base.spn_ovr_cmc,
    base.spn_commission_code,
    base.spn_det_commission_code,
    base.agent_code_cmp,
    base.agent_name,
    base.agent_code,
    base.agent_commission_code,
    base.YEAR,
    base.quarter,
    base.invc_number,
    base.inx_auto_key,
    base.ind_original,
    base.route_desc,
    base.inz_auto_key,
    base.invc_date_as_date,
    base.invc_date,
    base.sales_category,
    base.categories,
    fn_cmp_kyc_today(base.cmz_auto_key) AS kyc_today,
    fn_cmp_last_kyc_created(base.cmz_auto_key) AS last_kyc,
    fn_cmp_last_kyc_valid_till(base.cmz_auto_key) AS valid_till,
    CASE
        WHEN fn_cmp_kyc_for_invc(base.cmz_auto_key, base.invc_number) IS NOT NULL THEN fn_cmp_kyc_for_invc(base.cmz_auto_key, base.invc_number)
        WHEN base.cmp_parent IN (1, 2, 3) THEN 'VALID_KYC_Q'
        ELSE 'NO_KYC_Q'
    END AS kyc_q, 
    fn_spn_trans_validation(base.inx_auto_key) AS spn_no_of_transactions,
    fn_enm_trans_validation(base.inx_auto_key) AS enm_no_of_transactions,
    fn_warehouse_or_resold(base.stm_auto_key) AS sale_type,
    fn_index_agn(base.cmz_auto_key) AS indx_agn,
    fn_index_spn(base.cmz_auto_key) AS indx_spn,
    base.route_code,
    base.so_employee,
    base.invoice_route_code,
    base.post_desc,
    base.cmz_auto_key,
    base.company_name,
    base.cmp_parent,
    base.company_code,
    base.pn,
    base.description,
    base.item_number,
    base.qty_ship,
    base.unit_price,
    base.qty_ship * base.unit_price AS total_invoice,
    CASE 
        WHEN base.foreign_amount <> base.foreign_pmt_amt THEN base.qty_ship * base.unit_price
    END AS not_paid_revenue,
    base.unit_cost AS unit_cost,
    base.unit_price - base.unit_cost AS gp,
    base.unit_cost  * base.qty_ship AS total_cost, 
    (base.unit_price - base.unit_cost) * base.qty_ship AS total_gp, --does not include credit memo deductions
    base.spn_dtl_spn_code AS sales_person_code,
    base.ind_sales_person_name AS sales_person_name,
    base.spn_hdr_commission_code
FROM 
    base
LEFT JOIN credit_memo_exc ON base.inx_auto_key = credit_memo_exc.inx_auto_key
LEFT JOIN exc_type ON base.sox_auto_key = exc_type.sox_auto_key
LEFT JOIN pod_code ON base.sox_auto_key = pod_code.sox_auto_key
LEFT JOIN adj ON base.inx_auto_key = adj.inx_auto_key
LEFT JOIN credit_memo_ind ON base.inx_auto_key = credit_memo_ind.inx_auto_key
WHERE (1=1)
--AND base.year = 2021
--AND base.quarter = 1
AND base.sales_category IN (1, 2, 3, 4)
AND UPPER(base.route_desc) NOT LIKE ('CORE CREDIT')
;
