create or replace FUNCTION fn_spn_trans_validation (p_inx_auto_key IN INTEGER)
RETURN
INTEGER
AS no_of_transactions INTEGER;
v_inx_auto_key NUMBER := p_inx_auto_key;

BEGIN

SELECT
    CASE
        WHEN ( spn_ovr.sales_person_code = sys_invc_hd.employee_code
               OR spn_dtl.sales_person_code = sys_invc_hd.employee_code )
             AND ( spn_ovr.cmc_auto_key IN (1, 2)
                   OR spn_dtl.cmc_auto_key IN (1, 2) )
             OR spn_ovr.spn_auto_key = 0 THEN
            1 ELSE 0
    END
    +
    CASE
        WHEN ( spn_ovr.sales_person_code = emp_soh.employee_code
               OR spn_dtl.sales_person_code = emp_soh.employee_code )
             AND ( ( spn_ovr.cmc_auto_key IN (1, 2)
                     AND ind.cmc_override IN (1, 2) )
                   OR ( spn_dtl.cmc_auto_key IN (1, 2)
                        AND ind.cmc_auto_key IN (1, 2))) THEN
            1
        ELSE
            0
    END
    +
    CASE
        WHEN ( ind.cmc_override = 99
               AND ind.cmc_auto_key IS NULL )
             OR ( ind.cmc_override IS NULL
                  AND ind.cmc_auto_key = 99 )
             OR ( ind.cmc_override = 99
                  AND ind.cmc_auto_key NOT IN (1, 2) )
             OR ( ind.cmc_override NOT IN (1, 2)
                  AND ind.cmc_auto_key = 99 ) THEN
            1
        ELSE
            0
    END
    +
-- the logic used for the line above is to return true if there is no SPN assigned for transaction, 
--meaning it is not an error and should return 1
    CASE
        WHEN ( spn_ovr.sales_person_code = sys_cq_dtl.employee_code
               OR spn_dtl.sales_person_code = sys_cq_dtl.employee_code )
             AND ( ( spn_ovr.cmc_auto_key IN (1, 2)
                     AND ind.cmc_override IN (1, 2) )
                   OR ( spn_dtl.cmc_auto_key IN (1, 2)
                        AND ind.cmc_auto_key IN (1, 2) ) ) THEN
            1
        ELSE
            0 END
    +
            CASE
        WHEN ( spn_ovr.sales_person_code = sys_cq_hd.employee_code
               OR spn_dtl.sales_person_code = sys_cq_hd.employee_code )
             AND ( ( spn_ovr.cmc_auto_key IN (1, 2)
                     AND ind.cmc_override IN (1, 2) )
                   OR ( spn_dtl.cmc_auto_key IN (1, 2)
                        AND ind.cmc_auto_key IN (1, 2) ) ) THEN
            1
        ELSE
            0 END
    +

    CASE
        WHEN ( spn_ovr.sales_person_code = sys_sm_hd.employee_code
               OR spn_dtl.sales_person_code = sys_sm_hd.employee_code )
             AND ( ( spn_ovr.cmc_auto_key IN (1, 2)
                     AND ind.cmc_override IN (1, 2) )
                   OR ( spn_dtl.cmc_auto_key IN (1, 2)
                        AND ind.cmc_auto_key IN (1, 2) ) ) THEN
            1
        ELSE
            0
    END
    +
    CASE
        WHEN ( spn_ovr.sales_person_code = sys_po_hd.employee_code
               OR spn_dtl.sales_person_code = sys_po_hd.employee_code )
             AND ( ( spn_ovr.cmc_auto_key IN (1, 2)
                     AND ind.cmc_override IN (1, 2) )
                   OR ( spn_dtl.cmc_auto_key IN (1, 2)
                        AND ind.cmc_auto_key IN (1, 2) ) ) THEN
            1
        ELSE
            0
    END
    +
    CASE
        WHEN ( spn_ovr.sales_person_code = sys_vq_dtl.employee_code
               OR spn_dtl.sales_person_code = sys_vq_dtl.employee_code )
             AND ( ( spn_ovr.cmc_auto_key IN (1, 2)
                     AND ind.cmc_override IN (1, 2) )
                   OR ( spn_dtl.cmc_auto_key IN (1, 2)
                        AND ind.cmc_auto_key IN (1, 2) ) ) THEN
            1
        ELSE
            0
    END
    +
    CASE
        WHEN ( spn_ovr.sales_person_code = sys_ro_hd.employee_code
               OR spn_dtl.sales_person_code = sys_ro_hd.employee_code )
             AND ( ( spn_ovr.cmc_auto_key IN (1, 2)
                     AND ind.cmc_override IN (1, 2) )
                   OR ( spn_dtl.cmc_auto_key IN (1, 2)
                        AND ind.cmc_auto_key IN (1, 2) ) ) THEN
            1
        ELSE
            0
    END AS no_of_transactions
INTO 
    no_of_transactions
FROM
    invc_details ind
JOIN invc_head inh ON ind.inz_auto_key = inh.inz_auto_key
JOIN sys_user sys_invc_hd ON inh.sys_auto_key = sys_invc_hd.sys_auto_key
LEFT JOIN so_details sod ON ind.sox_auto_key = sod.sox_auto_key
LEFT JOIN so_head soh ON sod.soz_auto_key = soh.soz_auto_key
LEFT JOIN sales_person spn_ovr ON ind.spn_override = spn_ovr.spn_auto_key
LEFT JOIN sales_person spn_dtl ON ind.spn_auto_key = spn_dtl.spn_auto_key
LEFT JOIN sys_user emp_soh ON soh.sys_auto_key = emp_soh.sys_auto_key
LEFT JOIN cq_details ON sod.cqx_auto_key = cq_details.cqx_auto_key
LEFT JOIN cq_head ON cq_details.cqz_auto_key = cq_head.cqz_auto_key
LEFT JOIN sys_user sys_cq_dtl ON cq_details.sys_auto_key = sys_cq_dtl.sys_auto_key
LEFT JOIN sys_user sys_cq_hd ON cq_head.sys_auto_key = sys_cq_hd.sys_auto_key
LEFT JOIN sm_head ON inh.inz_auto_key = sm_head.inz_auto_key
LEFT JOIN sys_user sys_sm_hd ON sm_head.sys_auto_key = sys_sm_hd.sys_auto_key
LEFT JOIN po_head po_headr ON sm_head.poh_auto_key = po_headr.poh_auto_key
LEFT JOIN sys_user sys_po_hd ON po_headr.sys_auto_key = sys_po_hd.sys_auto_key
LEFT JOIN (SELECT 
                cqx_auto_key, 
                sys_auto_key, 
                selected_flag 
            FROM 
                vq_details 
            WHERE vq_details.selected_flag = 'T'
            AND ROWNUM = 1) vq_details ON cq_details.cqx_auto_key = vq_details.cqx_auto_key
LEFT JOIN sys_user sys_vq_dtl ON vq_details.sys_auto_key = sys_vq_dtl.sys_auto_key
LEFT JOIN ro_head ON sm_head.roh_auto_key = ro_head.roh_auto_key
LEFT JOIN sys_user sys_ro_hd ON ro_head.sys_auto_key = sys_ro_hd.sys_auto_key
WHERE (1=1)
AND inh.invoice_date > add_months(sysdate, -12)
AND ind.inx_auto_key = v_inx_auto_key;
RETURN no_of_transactions;
END;